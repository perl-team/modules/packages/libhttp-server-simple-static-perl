libhttp-server-simple-static-perl (0.14-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 23:14:00 +0100

libhttp-server-simple-static-perl (0.14-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!

  [ Damyan Ivanov ]
  * New upstream version 0.14
  * declare conformance with Policy 4.1.1 (no changes needed)
  * put myseld in Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Sun, 12 Nov 2017 15:14:39 +0000

libhttp-server-simple-static-perl (0.12-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nick Morrott ]
  * Import upstream version 0.12
  * Declare compliance with Debian Policy 3.9.8
  * Sort dependencies
  * Update metadata
  * Add more details to long description

  [ gregor herrmann ]
  * Update build dependencies.

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 12 Aug 2016 10:20:18 +0100

libhttp-server-simple-static-perl (0.09-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Mark package as autopkgtestable.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * Replace (versioned) dependencies on perl-modules with (unversioned)
    dependencies on libcgi-pm-perl.
  * Add Upstream-{Name,Contact} to debian/copyright.
  * Add debian/upstream/metadata.
  * Expand long package description.

 -- Axel Beckert <abe@debian.org>  Sun, 07 Jun 2015 00:23:19 +0200

libhttp-server-simple-static-perl (0.09-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ intrigeri ]
  * Imported Upstream version 0.09, that moves to File::LibMagic, so:
    - revert our Debian-specific temporary use of File::MMagic::XS
    - take into account this dependency change
  * Remove obsolete dependency on libmime-types-perl.
  * Add versioned dependency on perl-modules to get CGI.pm 3.46 or later.
  * Add new dependency on libhttp-date-perl.
  * Bump upstream copyright years.
  * Convert to machine-readable debian/copyright format 1.0
  * Declare compatibility with standards version 3.9.4.
  * Migrate to debhelper 9 with tiny rules.
  * De-duplicate build-dependency on perl.

 -- intrigeri <intrigeri@debian.org>  Tue, 20 Aug 2013 02:37:24 +0200

libhttp-server-simple-static-perl (0.07-2) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * Add debian/watch.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ intrigeri ]
  * Use 3.0 (quilt) source format.
  * Add "Copyright" word where needed.
  * Use File::MMagic::XS instead of File::MMagic to workaround #679600.
    This required a new patch:
    Use-File-MMagic-XS-instead-of-File-MMagic-to-workaro.patch

 -- intrigeri <intrigeri@debian.org>  Sat, 30 Jun 2012 07:32:55 +0200

libhttp-server-simple-static-perl (0.07-1) unstable; urgency=low

  * New upstream version.
  * First upload to Debian. (Closes: #497308)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Sun, 31 Aug 2008 13:41:37 -0400

libhttp-server-simple-static-perl (0.06-1) unstable; urgency=low

  * Initial non-release.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Thu, 25 Oct 2007 12:10:08 -0400
